import React, { useContext} from "react"
import { FruitContext } from "./fruitContext"
import axios from 'axios';

const FruitList = () => {
    const [dataBuah, setdataBuah, inputNama, setinputNama, inputHarga, setinputHarga, inputBerat, setinputBerat, selectedId, setselectedId] = useContext(FruitContext)

    console.log("dataBuah")
    console.log(dataBuah)
    console.log("dataBuah tes")

    const handleEdit = (event) => {
        let id = parseInt(event.target.value)
        console.log(id)
        let newDataBuah = dataBuah.find(x => x.id === id)
        console.log(newDataBuah)
        setinputNama(newDataBuah.name)
        setinputHarga(newDataBuah.price)
        setinputBerat(newDataBuah.weight)
        setselectedId(id)
      }
    
      const handleDelete = (event) => {
          let idDataBuah = parseInt(event.target.value)
  
          let newdaftarBuah = dataBuah.filter(el => el.id !== idDataBuah)
  
          axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`)
              .then(res => {
                  console.log(res)
              })
          setselectedId(-1)
          setdataBuah([...newdaftarBuah]) 
      }

    return (
        <center>
            <h1>Tabel Harga Buah</h1>
            <table style={{ width: "50%" }}>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataBuah !== null && dataBuah.map((buahObj, index) => {
                            return (
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>{buahObj.name}</td>
                                    <td>{buahObj.price}</td>
                                    <td>{buahObj.weight * 0.001} kg</td>
                                    <td>
                                        <button onClick={handleEdit} value={buahObj.id}>Edit</button>
                                        <button onClick={handleDelete} value={buahObj.id}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </center>
    )

}

export default FruitList