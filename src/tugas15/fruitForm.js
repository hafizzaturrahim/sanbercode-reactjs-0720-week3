import React, { useContext } from "react"
import { FruitContext } from "./fruitContext"
import axios from 'axios';
import './style.css';

const FruitForm = () => {
    const [dataBuah, setdataBuah, inputNama, setinputNama, inputHarga, setinputHarga, inputBerat, setinputBerat, selectedId, setselectedId] = useContext(FruitContext)

    console.log("data buah")
    console.log(dataBuah)

    const handleChange = (event) => {
        let typeofInput = event.target.name
        switch (typeofInput) {
            case "name": {
                setinputNama(event.target.value);
                break;
            }
            case "price": {
                setinputHarga(event.target.value);
                break;
            }
            case "weight": {
                setinputBerat(event.target.value);
                break;
            }
            default: { break; }
        }
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        let name = inputNama
        let price = inputHarga
        let weight = inputBerat


        if (name.replace(/\s/g, '') !== "" && price.replace(/\s/g, '').toString() !== "") {
            if (selectedId === -1) {
                axios.post(`http://backendexample.sanbercloud.com/api/fruits`, { name, price, weight })
                    .then(res => {
                        setdataBuah([
                            ...dataBuah,
                            { id: res.data.id, name, price, weight }])
                    })
            } else if (selectedId !== -1) {
                axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, { name, price, weight })
                    .then(res => {

                        console.log("edit")
                        console.log(res)
                    })
                let daftarBuah = dataBuah.find(el => el.id === selectedId)
                daftarBuah.name = name
                daftarBuah.price = price
                daftarBuah.weight = weight
                setdataBuah([...daftarBuah])
            }

            setselectedId(-1)
            setinputNama("")
            setinputHarga("")
            setinputBerat("")
        }

    }

    return (
        <>
            <center>
                {/* Form */}
                <h1>Form Buah</h1>
                <form onSubmit={handleSubmit}>
                    <table className="buah">
                        <tbody>
                            <tr>
                                <td><label>Masukkan nama buah:</label></td>
                                <td>:</td>
                                <td><input type="text" value={inputNama} name="name" onChange={handleChange} /></td>
                            </tr>
                            <tr>
                                <td><label>Masukkan harga</label></td>
                                <td>:</td>
                                <td><input type="text" value={inputHarga} name="price" onChange={handleChange} /></td>
                            </tr>
                            <tr>
                                <td><label>Masukkan berat (dalam gram)</label></td>
                                <td>:</td>
                                <td><input type="text" value={inputBerat} name="weight" onChange={handleChange} /></td>
                            </tr>
                            <tr>
                                <td><button>submit</button></td>
                            </tr>
                        </tbody>
                    </table>

                </form>
            </center>
        </>
    )

}

export default FruitForm