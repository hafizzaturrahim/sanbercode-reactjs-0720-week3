import React from "react"
import {FruitProvider} from "./fruitContext"
import FruitList from "./fruitList"
import FruitForm from "./fruitForm"

const Fruit = () =>{
  return(
    <FruitProvider>
      <FruitList/>
      <FruitForm/>
    </FruitProvider>
  )
}

export default Fruit