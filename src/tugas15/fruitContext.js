import React, { useState, useEffect, createContext } from "react";
import axios from 'axios';

export const FruitContext = createContext();
export const FruitProvider = props => {
    const [dataBuah, setdataBuah] = useState(null)
    const [inputNama, setinputNama] = useState("")
    const [inputHarga, setinputHarga] = useState("")
    const [inputBerat, setinputBerat] = useState("")
    const [selectedId, setselectedId] = useState(-1)
    
    useEffect(() => {
        if (dataBuah === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
                    .then(res => {
                        console.log("read data")
                        setdataBuah(res.data.map(el => {
                            return {
                                id: el.id,
                                name: el.name,
                                price: el.price,
                                weight: el.weight
                            }
                        }))
                        
                    })
        }

    }, [dataBuah])

    
    return (
        <FruitContext.Provider value={[dataBuah, setdataBuah, inputNama, setinputNama, inputHarga, setinputHarga, inputBerat, setinputBerat, selectedId, setselectedId]}>
            {props.children}
        </FruitContext.Provider>
    );
};