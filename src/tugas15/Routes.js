import React, { useContext, createContext } from "react";
import { Switch, Link, Route } from "react-router-dom";

import BuahInfo11 from '../tugas11/dataBuah';
import Timer from '../tugas12/timer';
import BuahInfo13 from '../tugas13/dataBuah';
import BuahInfo14 from '../tugas14/dataBuah';
import Fruit from '../tugas15/fruit';
import './theme.css';


const themes = {
  light: {
    foreground: "#000000",
    background: "crimson"
  },
  dark: {
    foreground: "#ffffff",
    background: "teal"
  }
};

const ThemeContext = createContext(themes.light);

const Routes = () => {
  let theme = useContext(ThemeContext);
  console.log("theme")
  console.log(theme.background)
  const toggleTheme = () => {
    // if the theme is not light, then set it to dark
    if (theme === 'light') {
      theme = 'dark'
    // otherwise, it should be light
    } else {
      theme = 'light'
    }
    console.log(theme)
  }
  return (
    <>
      <nav style={{background: theme.background}}>
      <ul style={{float:"left"}}>
         
        </ul>
        <ul>
          <li>
            <Link to="/context-list">List dengan context</Link>
          </li>

          <li>
            <Link to="/hook-lists">Lists dengan Hooks</Link>
          </li>
          <li>
            <Link to="/lists">Lists dengan Class</Link>
          </li>
          <li>
            <Link to="/timer">Timer</Link>
          </li>
          <li>
            <Link to="/">Simple List</Link>
          </li>
      <button onClick={toggleTheme}>Change color</button>
        </ul>
      </nav>

      <section>
      <Switch>
        <Route path="/timer">
        <Timer start={100}/>
        </Route>
        <Route path="/lists">
          <BuahInfo13 />
        </Route>
        <Route path="/hook-lists">
          <BuahInfo14 />
        </Route>
        <Route path="/context-list">
          <Fruit />
        </Route>
        <Route path="/">
          <BuahInfo11 />
        </Route>
      </Switch>
      </section>


    </>
  );
};

export default Routes;
