import React from 'react';
import './App.css';
import {BrowserRouter as Router} from "react-router-dom";
import Routes from './tugas15/Routes';

// import BuahInfo11 from './tugas11/dataBuah';
// import Timer from './tugas12/timer';
// import BuahInfo13 from './tugas13/dataBuah';
// import BuahInfo14 from './tugas14/dataBuah';


function App() {
  return (
    <div>
      <Router>
      <Routes/>
      </Router> 

      {/* <BuahInfo14 /> */}
      {/* <Timer start={100}/> */}

    </div>
  );
}

export default App;