import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './style.css';

const BuahInfo = () => {
  const [dataBuah, setdataBuah] = useState(null)
  const [inputNama, setinputNama] = useState("")
  const [inputHarga, setinputHarga] = useState("")
  const [inputBerat, setinputBerat] = useState("")
  const [indexOfForm, setIndexOfForm] = useState(-1)

  useEffect(() => {
    if(dataBuah === null){
      handleAPI()
    }

  }, [dataBuah])

  const handleAPI = () =>{
    axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        console.log("read data")
        console.log(res)
        setdataBuah(res.data.map(el => {
          return {
            id: el.id,
            nama: el.name,
            harga: el.price,
            berat: el.weight
          }
        }))
      })
  }

  const handleChange = (event) => {
    let typeofInput = event.target.name
    switch (typeofInput) {
      case "nama": {
        setinputNama(event.target.value);
        break;
      }
      case "harga": {
        setinputHarga(event.target.value);
        break;
      }
      case "berat": {
        setinputBerat(event.target.value);
        break;
      }
      default: { break; }
    }
  }

  const handleSubmit = (event) => {
    event.preventDefault()

    let nama = event.target.nama.value
    let harga = event.target.harga.value
    let berat = event.target.berat.value

    let newDataBuah = dataBuah
    let index = indexOfForm

    if (nama.replace(/\s/g, '') !== "" && harga.replace(/\s/g, '').toString() !== "" && berat.replace(/\s/g, '').toString() !== "") {
      if (index === -1) {
        //newDataBuah = [...newDataBuah, { nama, harga, berat }]
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, { name: nama, price: harga, weight: berat })
          .then(res => {
            console.log("tambah data")
            console.log(res)
            //ini bikin input jadi 2x kalo ditaruh di sini
            //handleAPI()
          })
          
      } else {
        let id = newDataBuah[index].id
        //newDataBuah[index] = { nama, harga, berat }
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${id}`, { name: nama, price: harga, weight: berat })
          .then(res => {
            console.log("edit data " +id)
            console.log(res)
            
          })
      }

      handleAPI()

      //setdataBuah(newDataBuah)
      setinputNama("")
      setinputHarga("")
      setinputBerat("")
      setIndexOfForm(-1)
    }
  }

  const handleEdit = (event) => {
    let index = event.target.value
    let newDataBuah = dataBuah[index]
    console.log(newDataBuah.id)

    setinputNama(newDataBuah.nama)
    setinputHarga(newDataBuah.harga)
    setinputBerat(newDataBuah.berat)
    setIndexOfForm(index)
  }

  const handleDelete = (event) => {
    let id = event.target.value
    let oriDataBuah = dataBuah

    if (oriDataBuah !== undefined) {
      axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
      .then(res => {
        console.log("hapus data " + id)
        console.log(res)
        handleAPI()
      })

      
      setIndexOfForm(-1)
    } 
  }

  return (
    <>
      <center>
        <h1>Tabel Harga Buah</h1>
        <table style={{ width: "50%" }}>
          <thead>
            <tr>
              <th>No.</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {
              dataBuah !== null && dataBuah.map((buahObj, index) => {
                return (
                  <tr>
                    <td>{index + 1}</td>
                    <td>{buahObj.nama}</td>
                    <td>{buahObj.harga}</td>
                    <td>{buahObj.berat * 0.001} kg</td>
                    <td>
                      <button onClick={handleEdit} value={index}>Edit</button>
                      <button onClick={handleDelete} value={buahObj.id}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>

        {/* Form */}
        <h1>Form Buah</h1>
        <form onSubmit={handleSubmit}>
          <table className="buah">
            <tbody>
              <tr>
                <td><label>Masukkan nama buah:</label></td>
                <td>:</td>
                <td><input type="text" value={inputNama} name="nama" onChange={handleChange} /></td>
              </tr>
              <tr>
                <td><label>Masukkan harga</label></td>
                <td>:</td>
                <td><input type="text" value={inputHarga} name="harga" onChange={handleChange} /></td>
              </tr>
              <tr>
                <td><label>Masukkan berat (dalam gram)</label></td>
                <td>:</td>
                <td><input type="text" value={inputBerat} name="berat" onChange={handleChange} /></td>
              </tr>
              <tr>
                <td><button>submit</button></td>
              </tr>
            </tbody>
          </table>

        </form>
      </center>
    </>
  )

}

export default BuahInfo