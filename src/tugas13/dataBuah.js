import React, { Component } from "react"
import './style.css';

let dataHargaBuah = [
  { nama: "Semangka", harga: 10000, berat: 1000 },
  { nama: "Anggur", harga: 40000, berat: 500 },
  { nama: "Strawberry", harga: 30000, berat: 400 },
  { nama: "Jeruk", harga: 30000, berat: 1000 },
  { nama: "Mangga", harga: 30000, berat: 500 }
]

class BuahInfo extends Component {

  constructor(props) {
    super(props)
    this.state = {
      dataBuah: dataHargaBuah,
      inputBuah: {},
      isEdit: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleChange(event) {
    let inputBuah = {...this.state.input}
    inputBuah[event.target.name] = event.target.value
    this.setState({ inputBuah});
    console.log(this.state.inputBuah)
  }

  handleSubmit(event) {
    event.preventDefault()

    let nama = event.target.nama.value
    let harga = event.target.harga.value
    let berat = event.target.berat.value

    let newDataBuah = this.state.dataBuah
    let indexForm = this.state.indexForm

    if (this.state.isEdit === false) {
      newDataBuah = [...this.state.dataBuah, { nama, harga, berat }]
    }else{
      newDataBuah[indexForm] = { nama, harga, berat }
    }

    this.setState({
      dataBuah: newDataBuah,
      inputBuah: "",
      isEdit: false
    })
  }

  handleEdit(event) {
    let index = event.target.value
    let newDataBuah = this.state.dataBuah[index]
    console.log(newDataBuah.nama)

    this.setState({ inputBuah: { nama: newDataBuah.nama, harga: newDataBuah.harga, berat: newDataBuah.berat }, isEdit: true, indexForm:index })
  }

  handleDelete(event){
    if(this.state.isEdit === false){
      let index = event.target.value
      let delDataBuah = this.state.dataBuah
      delDataBuah.splice(index,1)
      this.setState({dataBuah:delDataBuah,isEdit:false})
    }

  }

  render() {
    return (
      <>
        <center>
          <h1>Tabel Harga Buah</h1>
          <table style={{ width: "50%" }}>
            <thead>
              <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.dataBuah.map((buahObj, index) => {
                  return (
                    <tr>
                      <td>{index + 1}</td>
                      <td>{buahObj.nama}</td>
                      <td>{buahObj.harga}</td>
                      <td>{buahObj.berat * 0.001} kg</td>
                      <td>
                        <button onClick={this.handleEdit} value={index}>Edit</button>
                        <button onClick={this.handleDelete} value={index}>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>

          {/* Form */}
          <h1>Form Buah</h1>
          <form onSubmit={this.handleSubmit}>
            <table className="buah">
              <tbody>
                <tr>
                  <td><label>Masukkan nama buah:</label></td>
                  <td>:</td>
                  <td><input type="text" value={this.state.inputBuah.nama} name="nama" onChange={this.handleChange} /></td>
                </tr>
                <tr>
                  <td><label>Masukkan harga</label></td>
                  <td>:</td>
                  <td><input type="text" value={this.state.inputBuah.harga} name="harga" onChange={this.handleChange} /></td>
                </tr>
                <tr>
                  <td><label>Masukkan berat (dalam gram)</label></td>
                  <td>:</td>
                  <td><input type="text" value={this.state.inputBuah.berat} name="berat" onChange={this.handleChange} /></td>
                </tr>
                <tr>
                  <td><button>submit</button></td>
                </tr>
              </tbody>
            </table>

          </form>
        </center>
      </>
    )
  }
}

export default BuahInfo