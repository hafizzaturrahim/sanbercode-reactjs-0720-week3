import React from 'react';
import './style.css';

function UbahBerat(props) {
  let newBerat = props.berat * 0.001 + " kg";
  return <td>{newBerat}</td>
}

let dataHargaBuah = [
  { nama: "Semangka", harga: 10000, berat: 1000 },
  { nama: "Anggur", harga: 40000, berat: 500 },
  { nama: "Strawberry", harga: 30000, berat: 400 },
  { nama: "Jeruk", harga: 30000, berat: 1000 },
  { nama: "Mangga", harga: 30000, berat: 500 }
]

class BuahInfo extends React.Component {
  render() {
    return (
      <div>
        <center>
          <h1>Tabel Harga Buah</h1>
          <table style={{width:"50%"}}>
            <thead>
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
              </tr>
            </thead>
            {dataHargaBuah.map(el => {
              return (
                <tr>
                  <td>{el.nama}</td>
                  <td>{el.harga}</td>
                  <UbahBerat berat={el.berat}/> 
                </tr>
              )
            })}

          </table>
        </center>
      </div>
    )
  }
}

export default BuahInfo