import React, { Component } from 'react'

class Timer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            time: 0,
            date: new Date()
        }
    }

    componentDidMount() {
        if (this.props.start !== undefined) {
            this.setState({ time: this.props.start })
        }
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }
    componentDidUpdate() {
        if (this.state.time === 0) {
            clearInterval(this.timerID);
        }
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            time: this.state.time - 1,
            date: new Date()
        });
    }

    stopTimer() {

    }

    render() {
        return (
            <table className="timer">
                <tr>
                    {this.state.time > 0 &&
                        <>
                            <td><h2>sekarang jam {this.state.date.toLocaleTimeString()}</h2></td>
                            <td style={{ textAlign: "right" }}><h2>Hitung mundur: {this.state.time}</h2></td>
                        </>
                    }
                </tr>
            </table>
        )
    }
}

export default Timer